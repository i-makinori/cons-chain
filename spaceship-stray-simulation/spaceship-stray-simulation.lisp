
#|



|#

;;;; simulation symbol base

(defparameter *status-of-beginning*
'(
  "you are the explorer of the Solar System."
  "you are the member of the mission."
"you finished prove and experimental mining at Ploto."
"when you space-ship jump away from Gravity range of Plato."
"pulse pattern of electromagnetic of Plato changed suddenly."
"your space-ship loose the orbit to the Earth..."
  ))

;; (mapcar #'string-line-to-tokens  *status-of-beginning*)

(defparameter *current-status-dict*
  (append *orbit*))

(defparameter *orbit*
  '((order 太陽 (地球 月) (冥王星 衛生1 衛生2) 外部)))

(defparameter *spaceship*
  '((fuel low)
    (position (center 冥王星 外部))
    ))


(defparameter *order-of-empty-to-full*
  (list 'empty 'less 'low 'normal 'good 'full 'overflow 'blowdown)
  )


(defun graph-to-orbits (current-dimension orbit-function)
  ;; generate cost graph of fuel, food, stress, period, and so on to drive to position
  '()
  )

(defun format-rader (current-orbits)
  )


;;;;
    
(defun forward-season (methods current-status)
  )

(defun check-mission-complete (current-status)
  )

(defun check-the-death (current-status)
  )


;;;; symbol engine core

(defun display-status (status)
  (mapcar #'display status)
  )

(defparameter *separate-characters*
  '(#\Space #\, #\.))

(defun display (text)
  (format t "~A~%" text))


(defun string-line-to-tokens (string)
  (let ((symbol-list '())
        (position 0))
    (loop for i from 0 to (1- (length string))
          finally (setf symbol-list (cons (subseq string position i) symbol-list))
          do (cond ((member (aref string i) *separate-characters*)
                    (setf symbol-list (cons (subseq string position i) symbol-list))
                    (setf position (+ i 1)))
                   (t nil)))
    (mapcar #'read-from-string
            (remove "" (reverse symbol-list) :test #'string=))))



(defun read-line-to-tokens ()
  (read-line))
