# mate toy
### Material order and using simulator Toy


### 発想

### モデル

- 資材 A,B,C,D,E ... 製品 I,J,K ... がある。
- 製品Xを生産するのには、資材Yが C_XY 個必要である、
- 製品Xの要求生産量や、資材Yの予想価格は期間毎に変動する。
- 要求される生産量も期間毎に変動する。
- 製品の生産量・材料の価格・必要な資材の量が、それぞれ、月毎に表示される。


ある資材Xが、負の値になっていたら、ゲームオーバー。



### 課題など。

- 抽象化 (グローバル変数だらけの糞コードになっているので。)
- 製品の売却
- 在庫や腐敗の概念の実装
  ()
- 材料の生産限界
- マルチエージェントシステムへの変更。
  (材料も製品も、構造としては同じなので。)
- 市場の概念。
- 注文のキャンセル(される場合の実装)
- 新規の注文(月毎に、注文されている量の差分が表示される。)
- エージェントの実装   
  仕入れゲームを勝手プレイしてくれる。


### useage

```
USER> (repl-mate-toy-simu) ;; to start simulation

;; inner the simulation loop,
>> order order <name> <delta_period> <amount> 
;; to order material. such as,
>> order a 1 1000
>>
>> show-orders 
;; to show orders
>>
>> end-period
;; to end its period
```



