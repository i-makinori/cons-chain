;;
#|
;; material procurment toy game

|#

;;;; utils

(defparameter *separate-characters*
  '(#\Space #\, #\.))

(defun string-line-to-tokens (string)
  (let ((symbol-list '())
        (position 0))
    (loop for i from 0 to (1- (length string))
          finally (setf symbol-list (cons (subseq string position i) symbol-list))
          do (cond ((member (aref string i) *separate-characters*)
                    (setf symbol-list (cons (subseq string position i) symbol-list))
                    (setf position (+ i 1)))
                   (t nil)))
    (mapcar #'read-from-string
            (remove "" (reverse symbol-list) :test #'string=))))


;;;; materials

#|
(defun triangle-wave (cycle-time)
  (lambda (time)
    (let* ((effect
             (cond ((< (mod time cycle-time) (* cycle-time 1/4))
                    (+ 0 (mod time (/ cycle-time 4))))
                   ((< (mod time cycle-time) (* cycle-time 3/4))
                    (+ (/ cycle-time 4) (- (mod time (/ cycle-time 1)))))
                   (t 
                    (+ (- (/ cycle-time 4)) (mod time (/ cycle-time 4)))))))
      (/ effect (/ cycle-time 4))
)))
;;(loop for i from 0 to 30
;;               do (format t "~A: ~A~%" i  (funcall (triangle-wave 20) i)))
|#

(defparameter *cost-material-a*
  (lambda (period) (ceiling (+ 20 (* 3 (sin (* period 0.5)))))))
(defparameter *cost-material-b*
  (lambda (period) (ceiling (+ 15 (* 5 (sin (* period 0.8)))))))
(defparameter *cost-material-c*
  (lambda (period) (ceiling (+ 10 (* 0.1 (sin (* period 1)))))))
(defparameter *cost-material-d*
  (lambda (period) (ceiling (+ 5 (* 0 (sin (* period 1)))))))
(defparameter *cost-material-e*
  (lambda (period) (ceiling (+ 40 (* 20 (sin (* period 4)))))))

(defparameter *material-name-list*
  (list 'a 'b 'c 'd 'e))

(defparameter *demand-production-i*
  (lambda (period) period (ceiling (+ 100 (* 0 (sin (* period 4)))))))
(defparameter *demand-production-j*
  (lambda (period) period (ceiling (+ 20 (* 20 (sin (* period 2)))))))
(defparameter *demand-production-k*
  (lambda (period) period (ceiling (+ 14 (* 4 (sin (* period 4)))))))

(defparameter *demand-name-list*
  (list 'i 'j 'k))


;;(defun make-material-coefficient (production series-materials series-coefficients)
;;  production)

(defparameter *material-coefficient-i*
  (list 5 4 3 2 1))
(defparameter *material-coefficient-j*
  (list 10 20 30 20 10))
(defparameter *material-coefficient-k*
  (list 1 2 3 4 5))

#|
(defparameter *price-i*
  (lambda (period) period 1000))
(defparameter *price-j*
  (lambda (period) period 1500))
(defparameter *price-k*
  (lambda (period) period 2000))

(defparameter *price-list*
  (list *price-i* *price-j* *price-k*))

(defun sell-productions ())
|#


(defun consumed-material (number-for-product list-of-coefficient)
  (mapcar #'(lambda (coef) (* number-for-product coef))
          list-of-coefficient))


;;

;; historie and future
;;(defclass production-histfute ())

(defun make-histfute (&optional (length 24))
  (make-array length :element-type 'integer))

(defun attach-to-tail (array &optional (default 0))
  (concatenate `(vector ,(car (type-of array)))
               array (list default)))


;;

(defun format-predicts (period label delta_period- delta_period+ item-name-list perioded-things)
  (format t "~18,A:" label)
  (loop for p from delta_period- to delta_period+
        do (format t "~A~4,A"
                   (if (eql p 0) "!" " ")
                   (+ p period)))
  (format t "~%")
  (loop for i from 0 to (1- (length item-name-list))
        do (format t "~18,A:" (nth i item-name-list))
           (loop for p from delta_period- to delta_period+
                 do (format t "~5,A" (funcall (nth i perioded-things) (+ p period))))
           (format t "~%")))

(defun format-list-list (period label delta_period- delta_period+ item-name-list list-list)
  (format t "~18,A:" label)
  (loop for p from delta_period- to delta_period+
        do (format t "~A~4,A"
                   (if (eql p 0) "!" " ")
                   (+ p period)))
  (format t "~%")
  (loop for i from 0 to (1- (length item-name-list))
        do (format t "~18,A:" (nth i item-name-list))
           (loop for j from 0 to (1- (length (nth i list-list)))
                 do (format t "~5,A" (nth j (nth i list-list))))
           (format t "~%")))
  

;; (format-predicts 8 "(P)roductions" -3 8 *demand-name-list* *demand-list*)
;; (format-predicts 8 "(M)aterials" -3 8 *material-name-list* *material-list*)

(defun format-stock-current (item-name-list stock-list)
  (loop for i from 0 to (1- (length item-name-list))
        do (format t "~A:~A, " (nth i item-name-list) (nth i stock-list)))
  (format t "~%"))

;; (format-stock-current *material-name-list* *material-stock-list*)

;;;; simulator


(defparameter *current-period* 0)
(defparameter *credit-monery* 0)
(defparameter *material-stock-list*
  (list 100 100 100 100 100))
(defparameter *material-consumption-of-period*
  '())

(defparameter *material-list*
  (list *cost-material-a* *cost-material-b* *cost-material-c* *cost-material-d* *cost-material-e*))
(defparameter *demand-list*
  (list *demand-production-i* *demand-production-j* *demand-production-k*))
(defparameter *material-coefficient-list*
  (list *material-coefficient-i* *material-coefficient-j* *material-coefficient-k*))

(defparameter *order-list*
  '())



(defun init-simulator ()
  (setq *current-period* 0)
  (setq *credit-monery* 1000000)
  (setq *material-stock-list*
        (list 2000 2000 2000 2000 2000))
  (setq *material-consumption-of-period* '())
  (setq *order-list* '())
  )

;;


(defun add-alist (key number alist)
  ;; (add-alist 'a 10 '((c . 20) (a . 20)))
  (cond ((null alist) (list (cons key number)))
        ((eql key (caar alist))
         (cons (cons key (+ (cdar alist) number))
               (cdr alist)))
        (t
         (cons (car alist) (add-alist key number (cdr alist))))))


(defun insert-to-order-list (period name number order-list )
  ;; (insert-to-order-list 4 'a 100 '((1 (a . 100) (b . 200) (c . 300)) (4 (d . 200)) (11  (e . 13) (a . 20))))
  (cond ((or (null order-list) (< period (caar order-list)))
         (cons (cons period `((,name . ,number)))
               order-list))
        ((= period (caar order-list))
         (cons
          (cons (caar order-list) (add-alist name number (cdar order-list)))
          (cdr order-list)))
        (t
         (cons (car order-list)
               (insert-to-order-list period name number (cdr order-list))))))

(defun order-item (period item-name number price-predict)
  ;; (order-item 3 'b 20 *cost-material-b*)
  (let ((pay (* number (funcall price-predict period))))
    (setq *credit-monery* (- *credit-monery* pay))
    (setq *order-list*
          (insert-to-order-list  period item-name number *order-list*))))

(defun key-nth (key list &optional (memo 0))
  (cond ((null list) '())
        ((eql key (car list)) memo)
        (t (key-nth key (cdr list) (+ memo 1)))))

(defun nth-plus (n num list)
  (cond ((null list) '())
        ((= n 0) (cons (+ num (car list)) (cdr list)))
        (t (cons (car list) (nth-plus (- n 1) num (cdr list))))))

(defun update-item-supplie (period-current order-list)
  (let ((supply-this-month (assoc period-current order-list)))
    (mapcar #'(lambda (key-num)
                (let ((n (key-nth (car key-num) *material-name-list*)))
                  (when (numberp n)
                    (setf *material-stock-list*
                          (nth-plus n (cdr key-num) *material-stock-list*)))))
            (cdr supply-this-month))
    *material-stock-list*))

(defun parse-and-apply-order (args current-period)
  (let* ((what (car args))
         (delta (cadr args))
         (amount (caddr args))
         (what-nth (key-nth what *material-name-list*)))
    (cond ((and what-nth (not (null delta)) (not (null amount))
                (numberp delta) (numberp amount))
           (format t "you order ~A amount ~A for ~A(~A after) th period.~%"
                   what amount (+ current-period delta) delta)
           (order-item (+ current-period delta) what amount
                       (nth what-nth *material-list*)))
          (t (format t "order-failed.~%")
             (format t ":: order <name> <delta_period> <amount> ~%")))
    *order-list*))

(defun show-orders (order-list current-period)
  (let* ((order-for-format
           (remove-if #'(lambda (m-os)
                          (<= (car m-os) current-period))
                      order-list)))
    (print order-for-format) (terpri)
    ))

(defun format-current-money ()
  (format t "current monery: ~A~%" *credit-monery*))

(defun format-current-stocks ()
  (format t "resting stocks: ")
  (format-stock-current *material-name-list* *material-stock-list*))

(defparameter *help-text*
  "order <symbol> <delta-period> <amount>
show-orders
current-money
current-stocks
rereport
help
end-period
quit
h || help
")

(defun help ()
  (format t *help-text*))

(defun repl-of-period (status-of-period)
    ;; read eval print loop of period
  ;; your period's choices
  (princ ">> ")
  (let* ((tokens (string-line-to-tokens (read-line)))
         (func (car tokens))
         (args (cdr tokens))
         (reperiod 't))
    args
    (cond
      ((eq func 'quit)
       (format t "bye.~%")
       (setq reperiod #'(lambda () nil)))
      ((eq func 'order)
       (parse-and-apply-order args *current-period*))
      ((eq func 'show-orders)
       (show-orders *order-list* *current-period*))
      ((eq func 'end-period)
       (setq reperiod #'(lambda () (loop-simulator status-of-period))))
      ((eq func 'current-money)
       (format-current-money))
      ((eq func 'current-stocks)
       (format-current-stocks))
      ((eq func 'rereport)
       (report-of-period '()))
      ((or (eq func 'h) (eq func 'help))
       (help))
      (t
       (format t "unknown command, h to help.~%")))
    (if (eql reperiod t)
        (repl-of-period (identity status-of-period))
        (funcall reperiod))))


(defun needed-materials (period coefficient-list demand-function-list)
  (let* ((consumed-per-production-list
           (loop for i from 0 to (1- (length demand-function-list))
                 collect (let (;;(production-name (nth i *demand-name-list*))
                               (consumed
                                 (consumed-material (funcall (nth i demand-function-list) period)
                                                    (nth i coefficient-list))))
                           ;;(format t "consumed for ~A: ~A~%" production-name consumed)
                           consumed)))
         (consumed (apply #'mapcar #'+ consumed-per-production-list)))
    consumed))

(defun updation-of-period (status)
  status
  ;;
  (setq *current-period* (+ *current-period* 1))
  ;; consumed
  (let* ((consumed (needed-materials *current-period* *material-coefficient-list* *demand-list*)))
    ;;(format t "whole consumed: ~A~%" consumed)
    (setq *material-consumption-of-period* consumed)
    (setq *material-stock-list*
          (mapcar #'-
                  *material-stock-list*
                  consumed)))
  nil
  ;; supplied
  (update-item-supplie *current-period* *order-list*)
  )

(defun transpose (list-of-lists)
  (apply #'mapcar #'list list-of-lists))


(defun report-of-period (status)
  status
  ;;
  (format t "~%~%")
  (format t "the ~A th period~%"  *current-period*)
  (format-current-money)
  ;;
  (format t "whole consumed: ~A~%" *material-consumption-of-period*)
  (format-current-stocks)
  ;;
  (let* ((delta1 0) (delta2 8))
    ;;
    (format t "Demanded Production Predicts: ~%")
    (format-predicts *current-period*
                     "(P)roductions" delta1 delta2 *demand-name-list* *demand-list*)
    ;;
    (format t "Price predicts of Materials: ~%")
    (format-predicts *current-period*
                     "(M)aterials" delta1 delta2 *material-name-list* *material-list*)
    ;;
    (format t "Needed Material Predicts: ~%")
    ;;(format-predicts
    ;;*current-period* "(N)will Needed" delta1 delta2 *material-name-list*
    (format-list-list
     *current-period* "(W)ill needed" delta1 delta2 *material-name-list*
     (transpose (loop for i from delta1 to delta2
                      collect (needed-materials (+ i *current-period*)
                                                *material-coefficient-list*
                                                *demand-list*))))))




(defun loop-simulator (status)
  ;; update period
  (updation-of-period status)
  ;; report of period
  (report-of-period status)
  ;;
  (repl-of-period status)

  )

(defun repl-mate-toy-simu ()
  (init-simulator)
  (loop-simulator '())
  )

(defun repl-continue ()
  ;; (init-simulator)
  (loop-simulator '()))
